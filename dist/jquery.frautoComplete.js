/*
 * FrautoComplete
 *
 * Turns any text field into an autocomplete field,
 * given a list to populate from.
 *
 * @param {jQuery Element} targetField
 *   The field to autocomplete
 * @param {jQuery Element} obj.hiddenField
 *   A hidden field to accept additional data
 * @param {Object} obj.handlers
 *   An object of callback methods. 
 * @param {Number} obj.listLimit
 *   # of items to show in list.
 * @param {Boolean} obj.validate
 *   When true, fC will check if input is in list on blur.
 * @param {Boolean} obj.showModel
 *   When true, productTemplate will include model name & SKU.
 * @param {Array} obj.list
 *   The list of autocomplete options
 *
 * @return {Object} Returns public methods
 *   .updateList()
 *   .getList()
 */

'use strict';

var FrautoComplete = function FrautoComplete(targetField) {
  var _ref = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

  var _ref$hiddenField = _ref.hiddenField;
  var hiddenField = _ref$hiddenField === undefined ? targetField : _ref$hiddenField;
  var _ref$handlers = _ref.handlers;
  var handlers = _ref$handlers === undefined ? {} : _ref$handlers;
  var _ref$listLimit = _ref.listLimit;
  var listLimit = _ref$listLimit === undefined ? 5 : _ref$listLimit;
  var _ref$validate = _ref.validate;
  var validate = _ref$validate === undefined ? true : _ref$validate;
  var _ref$showModel = _ref.showModel;
  var showModel = _ref$showModel === undefined ? false : _ref$showModel;
  var _ref$list = _ref.list;
  var list = _ref$list === undefined ? [] : _ref$list;

  /*
   * State
   */

  var state = {
    targetField: targetField,
    hiddenField: hiddenField,
    handlers: handlers,
    listLimit: listLimit,
    validate: validate,
    showModel: showModel,
    list: list,
    targetContainer: targetField.parent(),
    listContainer: $('<ul class="frauto-list" />')
  };

  /*
   * Event Handlers
   */

  (function (state) {
    // Make selection on click
    state.targetContainer.on('mousedown', '.frauto-list li', function (event) {
      // preventDefault stops the blur event from triggering
      event.preventDefault();
      state.targetContainer.focus();
      makeSelection($(this), state);
      hideList(state);
    });

    // Press Enter to select the highlighted item
    state.targetField.on('keydown', function (event) {
      var $selection = $('.frauto-list').find('.selected');

      if (event.which === 13) {
        event.preventDefault();
        makeSelection($selection, state);
        hideList(state);
      }
    });

    // Keyboard list controls
    state.targetContainer.on('keyup', state.targetField, function (event) {
      switch (event.which) {
        // esc
        case 27:
          hideList(state);
          break;

        // down
        case 40:
          cycle('next');
          break;

        // up
        case 38:
          cycle('prev');
          break;

        // all other keys should rebuild the list
        default:
          createDropdown(state);
          showList(state);
      }
    });

    // Test for mousedown event to allow
    // scrolling with click&drag in IE
    var clickTest = undefined;
    state.targetContainer.on('mousedown', 'ul', function (event) {
      clickTest = event.target;
    });

    // Blur event
    state.targetField.on('blur', function (event) {
      // Skip this function if blur is caused by
      // clicking on scrollbar in IE
      if (clickTest && $(clickTest)[0].nodeName === 'UL') return true;
      onBlur(state);
    });

    // Remove selected class on hover
    state.targetContainer.on('hover', 'li', function (event) {
      $('.frauto-list').find('.selected').removeClass('selected');
    });
  })(state);

  /*
   * DOM Methods
   */

  // Hide the dropdown list
  var hideList = function hideList(state) {
    return state.targetContainer.find('.frauto-list').hide();
  };

  // Show the dropdown list
  var showList = function showList(state) {
    return state.targetContainer.find('.frauto-list').show();
  };

  // Clear input in fields
  var clearField = function clearField(state) {
    state.targetField.val('');
    state.hiddenField.val('');
  };

  // Create list item markup
  var listItemMarkup = function listItemMarkup(string) {
    var value = arguments.length <= 1 || arguments[1] === undefined ? string : arguments[1];
    return (function () {
      return ['<li data-value="', value, '">', string, '</li>'].join('');
    })();
  };

  // Create markup for list items
  var listMarkup = function listMarkup(filteredList) {
    return filteredList.reduce(function (markup, item, i) {
      markup += listItemMarkup(item.string, item.value);
      return markup;
    }, '');
  };

  // Create and attach the dropdown DOM element
  var createDropdown = function createDropdown(state) {
    var input = state.targetField.val();
    var isProduct = state.hiddenField.attr('id') === 'product';

    // Reset the DOM list
    state.listContainer.children().remove();

    // Filter the list
    var filteredList = filterList(input, isProduct);

    // Create the items as DOM elements
    var markup = listMarkup(filteredList);
    state.listContainer.append(markup);
    state.listContainer.find('li').first().addClass('selected');

    // Attach the list to the DOM
    $('.frauto-list').remove();
    state.targetField.after(state.listContainer);

    // Hide the list by default
    hideList(state);
    if (input.length) showList(state);
  };

  // Select an item from the list
  var makeSelection = function makeSelection(selection, state) {
    var show = selection.text();
    var hide = selection.attr('data-value');

    // Set field values
    state.targetField.val(show);
    state.hiddenField.val(hide);

    // Update selected item
    $('.frauto-list > li').removeClass('selected');
    selection.addClass('selected');

    // Validate field if necessary
    if (state.targetField.hasClass('invalid')) {
      state.targetField.valid();
    }

    // Do onSelection handler
    if (state.handlers.hasOwnProperty('onSelection')) {
      state.handlers.onSelection(selection);
    }
  };

  // Cycle through the list
  var cycle = function cycle(dir) {
    var $list = state.listContainer;
    var $current = $list.find('.selected');
    var $prev = $current.prev();
    var $next = $current.next();
    var $first = $list.find('li').first();
    var $last = $list.find('li').last();
    var selection = $current;

    // Make sure list is displayed
    showList(state);

    if (dir === 'prev') {
      if ($prev.length) {
        selection = $prev;
      } else {
        selection = $last;
      }
    } else {
      if ($next.length) {
        selection = $next;
      } else {
        selection = $first;
      }
    }

    // Scroll to the new selection if necessary
    $list.scrollTop($list.scrollTop() + selection.position().top - ($list.height() / 2 + selection.height() / 2));

    makeSelection(selection, state);
  };

  // Handle blur event
  var onBlur = function onBlur(state) {
    var $listItems = state.targetContainer.find('li');

    // Init vals for matchInput
    var show = state.targetField.val();
    var hiddenFieldId = state.hiddenField.attr('id');

    // Weird, but don't continue if target field
    // is still focused
    if (state.targetField.is(':focus')) return true;

    // Select visible option if only one is available
    if ($listItems.length === 1) {
      makeSelection($listItems.first(), state);
    }

    // Validate the input
    if (state.validate) {
      var match = matchInput({ show: show, hiddenFieldId: hiddenFieldId });
      if (!match) clearField(state);
    }

    hideList(state);
  };

  /*
   * Data Methods
   */

  // Output list item string & distinct values
  var itemTemplate = function itemTemplate(item) {
    var value = arguments.length <= 1 || arguments[1] === undefined ? item : arguments[1];
    return (function () {
      return {
        string: typeof item === 'string' ? item : item.label,
        value: value
      };
    })();
  };

  // Output product list item string & distinct values
  var productTemplate = function productTemplate(product) {
    var showModel = arguments.length <= 1 || arguments[1] === undefined ? state.showModel : arguments[1];

    var product_id = product.product_id || product.id;
    var product_sku = product.product_sku || product.sku;
    var product_name = product.product_name || product.label;

    var string = product_sku;
    if (showModel) string = [product_name, ' (', product_sku, ')'].join('');

    return {
      string: string,
      value: product_id
    };
  };

  // Filter the list with above templates
  var filterList = function filterList(val) {
    var isProduct = arguments.length <= 1 || arguments[1] === undefined ? false : arguments[1];

    // Return array of template objects
    return state.list.reduce(function (filtered, item, i) {
      // Exit loop when limit reached
      if (i >= state.listLimit) return false;

      // Get the appropriate data template
      var listItem = isProduct ? productTemplate(item) : itemTemplate(item);

      // Filter & add template objects to array
      if (fuzzyMatch(val, listItem.string)) {
        filtered.push(listItem);
      }

      return filtered;
    }, []);
  };

  // Match without spaces, punctuation, or other special chars
  var fuzzyMatch = function fuzzyMatch(val, string) {
    var regex = /[\'\"\-\[\]\/\{\}\(\)\*\+\?\!\.\,\\\^\$\|\ ]/g;
    var escVal = val.replace(regex, '').toUpperCase();
    var escStr = string.replace(regex, '').toUpperCase();
    var search = new RegExp(escVal, 'i');

    return search.test(escStr);
  };

  // Validate selection and entry
  var matchInput = function matchInput(_ref2) {
    var show = _ref2.show;
    var hiddenFieldId = _ref2.hiddenFieldId;

    var match = false;
    var string = '';

    // Don't run if input is blank
    if (!show) return false;

    // Match on simple lists
    if (typeof state.list[0] === 'string') {
      if (state.list.indexOf(show) >= 0) match = true;

      // Match on lists of objects
    } else {
        match = state.list.some(function (listItem) {
          // Set matching string
          if (hiddenFieldId === 'product') {
            string = productTemplate(listItem).string;
          }

          if (hiddenFieldId === 'purchased_at') {
            string = itemTemplate(listItem.value).string;
          }

          // Exit Array#some when match is found
          return string && show === string;
        });
      }

    return match;
  };

  /*
   * Public Methods
   */

  // Update the list
  var _updateList = function _updateList(list) {
    // TODO: Cache the list and return
    // if new list === state.list
    state.list = list;
    createDropdown(state);
  };

  // Get the list
  var _getList = function _getList(state) {
    return {
      getList: function getList() {
        return state.list;
      }
    };
  };

  // Return a set of public methods
  return {
    updateList: function updateList(newList) {
      return _updateList(newList);
    },
    getList: function getList() {
      return _getList(state).getList();
    }
  };
};
/*
 * FrautoComplete jQuery Method
 *
 * Usage example:
 *   var list = ['my option', 'another option', 'so many option'];
 *   $('input').frautoComplete(list);
 *
 *   -- OR --
 *
 *   var myFrauto = $('input').frautoComplete();
 *   ... later ...
 *   var list = [...];
 *   myFrauto.updateList(list);
 *
 * @param {Array} list
 *   The list of objects or strings to autocomplete
 * @param {Object} settings
 *   Settings object, same as obj param in FrautoComplete
 */
jQuery.fn.frautoComplete = function (list) {
  var settings = arguments.length <= 1 || arguments[1] === undefined ? { listLimit: 5 } : arguments[1];

  var targetField = $(this);

  // Set options on DOM element
  if (targetField.attr('data-allowfreeinput') === 'true') {
    settings.validate = false;
  }

  if (targetField.attr('data-showmodelname') === 'true') {
    settings.showModelName = true;
  }

  // instantiate
  var frauto = FrautoComplete(targetField, settings);
  if (list) frauto.updateList(list);

  // Allow chaining of public methods
  return frauto;
};