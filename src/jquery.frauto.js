/*
 * FrautoComplete jQuery Method
 *
 * Usage example:
 *   var list = ['my option', 'another option', 'so many option'];
 *   $('input').frautoComplete(list);
 *
 *   -- OR --
 *
 *   var myFrauto = $('input').frautoComplete();
 *   ... later ...
 *   var list = [...];
 *   myFrauto.updateList(list);
 *
 * @param {Array} list
 *   The list of objects or strings to autocomplete
 * @param {Object} settings
 *   Settings object, same as obj param in FrautoComplete
 */
jQuery.fn.frautoComplete = function (list) {
  var settings = arguments.length <= 1 || arguments[1] === undefined ? { listLimit: 5 } : arguments[1];

  var targetField = $(this);

  // Set options on DOM element
  if (targetField.attr('data-allowfreeinput') === 'true') {
    settings.validate = false;
  }

  if (targetField.attr('data-showmodelname') === 'true') {
    settings.showModelName = true;
  }

  // instantiate
  var frauto = FrautoComplete(targetField, settings);
  if (list) frauto.updateList(list);

  // Allow chaining of public methods
  return frauto;
};