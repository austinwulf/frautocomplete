/*
 * FrautoComplete
 *
 * Turns any text field into an autocomplete field,
 * given a list to populate from.
 *
 * @param {jQuery Element} targetField
 *   The field to autocomplete
 * @param {jQuery Element} obj.hiddenField
 *   A hidden field to accept additional data
 * @param {Object} obj.handlers
 *   An object of callback methods. 
 * @param {Number} obj.listLimit
 *   # of items to show in list.
 * @param {Boolean} obj.validate
 *   When true, fC will check if input is in list on blur.
 * @param {Boolean} obj.showModel
 *   When true, productTemplate will include model name & SKU.
 * @param {Array} obj.list
 *   The list of autocomplete options
 *
 * @return {Object} Returns public methods
 *   .updateList()
 *   .getList()
 */

const FrautoComplete =
(targetField, {
  hiddenField = targetField,
  handlers = {}, 
  listLimit = 5,
  validate = true,
  showModel = false,
  list = []
} = {}) => {

  /*
   * State
   */

  let state = {
    targetField,
    hiddenField,
    handlers,
    listLimit,
    validate,
    showModel,
    list,
    targetContainer: targetField.parent(),
    listContainer: $('<ul class="frauto-list" />')
  };

  /*
   * Event Handlers
   */

  ((state) => {
    // Make selection on click
    state.targetContainer.on('mousedown', '.frauto-list li', function(event) {
      // preventDefault stops the blur event from triggering
      event.preventDefault();
      state.targetContainer.focus();
      makeSelection($(this), state);
      hideList(state);
    });

    // Press Enter to select the highlighted item
    state.targetField.on('keydown', function(event) {
      let $selection = $('.frauto-list').find('.selected');

      if (event.which === 13) {
        event.preventDefault();
        makeSelection($selection, state);
        hideList(state);
      }
    });

    // Keyboard list controls
    state.targetContainer.on('keyup', state.targetField, function(event) {
      switch (event.which) {
        // esc
        case 27:
          hideList(state);
          break;

        // down
        case 40:
          cycle('next');
          break;

        // up
        case 38:
          cycle('prev');
          break;

        // all other keys should rebuild the list
        default:
          createDropdown(state);
          showList(state);
      }
    });

    // Test for mousedown event to allow
    // scrolling with click&drag in IE
    let clickTest;
    state.targetContainer.on('mousedown', 'ul', function(event) {
      clickTest = event.target;
    });

    // Blur event
    state.targetField.on('blur', function(event) {
      // Skip this function if blur is caused by
      // clicking on scrollbar in IE
      if (clickTest && $(clickTest)[0].nodeName === 'UL') return true;
      onBlur(state);
    });

    // Remove selected class on hover
    state.targetContainer.on('hover', 'li', function(event) {
      $('.frauto-list').find('.selected').removeClass('selected');
    });
  })(state);

  /*
   * DOM Methods
   */

  // Hide the dropdown list
  const hideList = (state) => state.targetContainer.find('.frauto-list').hide();

  // Show the dropdown list
  const showList = (state) => state.targetContainer.find('.frauto-list').show();

  // Clear input in fields
  const clearField = (state) => {
    state.targetField.val('');
    state.hiddenField.val('');
  };

  // Create list item markup
  const listItemMarkup = (string, value = string) => [
    '<li data-value="', value, '">', string, '</li>'].join('');

  // Create markup for list items
  const listMarkup = (filteredList) => {
    return filteredList.reduce((markup, item, i) => {
      markup += listItemMarkup(item.string, item.value);
      return markup;
    }, '');
  };

  // Create and attach the dropdown DOM element
  const createDropdown = (state) => {
    let input = state.targetField.val();
    let isProduct = state.hiddenField.attr('id') === 'product';

    // Reset the DOM list
    state.listContainer.children().remove();

    // Filter the list
    let filteredList = filterList(input, isProduct);

    // Create the items as DOM elements
    let markup = listMarkup(filteredList);
    state.listContainer.append(markup);
    state.listContainer.find('li').first().addClass('selected');

    // Attach the list to the DOM
    $('.frauto-list').remove();
    state.targetField.after(state.listContainer);

    // Hide the list by default
    hideList(state);
    if (input.length) showList(state);
  };

  // Select an item from the list
  const makeSelection = (selection, state) => {
    let show = selection.text();
    let hide = selection.attr('data-value');

    // Set field values
    state.targetField.val(show);
    state.hiddenField.val(hide);

    // Update selected item
    $('.frauto-list > li').removeClass('selected');
    selection.addClass('selected');

    // Validate field if necessary
    if (state.targetField.hasClass('invalid')) {
      state.targetField.valid(); }

    // Do onSelection handler
    if (state.handlers.hasOwnProperty('onSelection')) {
      state.handlers.onSelection(selection); }
  };

  // Cycle through the list
  const cycle = (dir) => {
    let $list = state.listContainer;
    let $current = $list.find('.selected');
    let $prev = $current.prev();
    let $next = $current.next();
    let $first = $list.find('li').first();
    let $last = $list.find('li').last();
    let selection = $current;

    // Make sure list is displayed
    showList(state);

    if (dir === 'prev') {
      if ($prev.length) {
        selection = $prev;
      } else {
        selection = $last;
      }
    } else {
      if ($next.length) {
        selection = $next;
      } else {
        selection = $first;
      }
    }

    // Scroll to the new selection if necessary
    $list.scrollTop(
      ($list.scrollTop() + selection.position().top) -
      ($list.height()/2 + selection.height()/2)
    );

    makeSelection(selection, state);
  };

  // Handle blur event
  const onBlur = (state) => {
    let $listItems = state.targetContainer.find('li');

    // Init vals for matchInput
    let show = state.targetField.val();
    let hiddenFieldId = state.hiddenField.attr('id');

    // Weird, but don't continue if target field
    // is still focused
    if (state.targetField.is(':focus')) return true;

    // Select visible option if only one is available
    if ($listItems.length === 1) {
      makeSelection($listItems.first(), state); }

    // Validate the input
    if (state.validate) {
      let match = matchInput({ show, hiddenFieldId });
      if (!match) clearField(state);
    }

    hideList(state);
  };

  /*
   * Data Methods
   */

  // Output list item string & distinct values
  const itemTemplate = (item, value = item) => ({
    string: typeof item === 'string' ? item : item.label,
    value
  });

  // Output product list item string & distinct values
  const productTemplate = (product, showModel = state.showModel) => {
    let product_id = product.product_id || product.id;
    let product_sku = product.product_sku || product.sku;
    let product_name = product.product_name || product.label;

    let string = product_sku;
    if (showModel) string = [product_name, ' (', product_sku, ')'].join('');

    return {
      string,
      value: product_id
    };
  };

  // Filter the list with above templates
  const filterList = (val, isProduct = false) => {
    // Return array of template objects
    return state.list.reduce((filtered, item, i) => {
      // Exit loop when limit reached
      if (i >= state.listLimit) return false;

      // Get the appropriate data template
      let listItem = isProduct ? productTemplate(item) : itemTemplate(item);

      // Filter & add template objects to array
      if (fuzzyMatch(val, listItem.string)) {
        filtered.push(listItem); }

      return filtered;
    }, []);
  };

  // Match without spaces, punctuation, or other special chars
  const fuzzyMatch = (val, string) => {
    let regex = /[\'\"\-\[\]\/\{\}\(\)\*\+\?\!\.\,\\\^\$\|\ ]/g;
    let escVal = val.replace(regex, '').toUpperCase();
    let escStr = string.replace(regex, '').toUpperCase();
    let search = new RegExp(escVal, 'i');

    return search.test(escStr);
  };

  // Validate selection and entry
  const matchInput = ({ show, hiddenFieldId }) => {
    let match = false;
    let string = '';

    // Don't run if input is blank
    if (!show) return false;

    // Match on simple lists
    if (typeof state.list[0] === 'string') {
      if (state.list.indexOf(show) >= 0) match = true;

    // Match on lists of objects
    } else {
      match = state.list.some((listItem) => {
        // Set matching string
        if (hiddenFieldId === 'product') {
          string = productTemplate(listItem).string; }

        if (hiddenFieldId === 'purchased_at') {
          string = itemTemplate(listItem.value).string; }

        // Exit Array#some when match is found
        return string && show === string;
      });
    }

    return match;
  };

  /*
   * Public Methods
   */

  // Update the list
  const updateList = (list) => {
    // TODO: Cache the list and return
    // if new list === state.list
    state.list = list;
    createDropdown(state);
  };

  // Get the list
  const getList = (state) => ({
    getList: () => state.list
  });

  // Return a set of public methods
  return {
    updateList: (newList) => updateList(newList),
    getList: () => getList(state).getList()
  };

};
