/**
 * Registria Gulp Boilerplate
 *
 * Use this when starting a new project.
 * Don't forget to rename the project in
 * your pacakage.json!
 *
 * Set the variables below to customize
 * the setup for your project.
 */

// Config
var project = {
  name: 'frautoComplete',
  version: '1.8.0',
  paths: {
    src: 'src/',
    lib: 'lib/',
    dist: 'dist/'
  }
};

// Include your scripts in order
var jsModules = [
  project.paths.src + 'frauto.js'
];

// Give each master Sass file a name
// Ex: mySass: project.paths.src + 'css/mySass.scss'
var sassModules = {
  style: project.paths.src + 'css/style.scss'
};

/**
 * Black box stuff ahead...
 */

// Init Gulp Modules
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var babel = require('gulp-babel');
var run = require('run-sequence');

// JavaScript Tasks
gulp.task('lint', function(){
  return gulp.src(jsModules)
    .pipe(jshint({ esnext: true }))
    .pipe(jshint.reporter('default'));
    // TODO: Include the Registria .eslint file
});

gulp.task('frauto', function(){
  return gulp.src(jsModules)
    .pipe(concat(project.name + '-' + project.version + '.js'))
    .pipe(babel())
    .pipe(gulp.dest(project.paths.dist))
    .pipe(uglify())
    .pipe(rename({extname: '.min.js'}))
    .pipe(gulp.dest(project.paths.dist));
});

gulp.task('jquery', function(){
  return gulp.src([
    project.paths.dist + project.name + '-' + project.version + '.js',
    project.paths.src + 'jquery.frauto.js'
  ])
    .pipe(concat('jquery.' + project.name + '.js'))
    .pipe(gulp.dest(project.paths.dist))
    .pipe(uglify())
    .pipe(rename({extname: '.min.js'}))
    .pipe(gulp.dest(project.paths.dist));
});

gulp.task('scripts', function(){
  run(
    'frauto',
    'jquery'
  );
});

gulp.task('watch', function(){
    gulp.watch(project.paths.src + '*.js', ['scripts']);
});

// Default task
gulp.task('default', ['scripts', 'watch']);